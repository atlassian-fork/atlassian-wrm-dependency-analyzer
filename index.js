const fs = require('fs-extra');
const path = require('path');
const {ExternalDependenciesSizeCalculator} = require('atlassian-wrm-external-dependencies-size-calculator');

const externalDependenciesCalculator = new ExternalDependenciesSizeCalculator();

function getStaticReportTemplate() {
    return fs.readFileSync(
        path.join(__dirname, 'dist', 'index.html'),
        'utf-8'
    );
}

const getStaticReportHTML = async (WRMReport, baseUrl) => {
    const reports = await externalDependenciesCalculator.calculateReport(WRMReport, baseUrl);
    const printableReports = externalDependenciesCalculator.toPrintable(reports);
    
    const staticReportTemplate = getStaticReportTemplate();

    return staticReportTemplate
        .replace(/REPLACE_FOR_ENTRY_POINT_REPORT/, JSON.stringify(printableReports.entryPoints))
        .replace(/REPLACE_FOR_SIZES_REPORT/, JSON.stringify(printableReports.sizes))
        .replace(/REPLACE_FOR_EXTERNAL_SIZES_REPORT/, JSON.stringify(printableReports.externalSizes))
        .replace(/REPLACE_FOR_DEPENDENCIES_REPORT/, JSON.stringify(printableReports.dependencies));
};

module.exports = {
    getStaticReportHTML
};