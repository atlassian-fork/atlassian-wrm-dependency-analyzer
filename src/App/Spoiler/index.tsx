import React, { Component } from 'react';
import ChevronDownIcon from '@atlaskit/icon/glyph/chevron-down';

import './style.less';

interface ISpoilerProps {
    label: string;
}

interface ISpoilerState { 
    open: boolean;
}

export default class Spoiler extends Component<ISpoilerProps, ISpoilerState> {
    state = {
        open: false
    }

    render() {
        const { label, children } = this.props;
        const { open } = this.state;

        return (
            <div className={`spoiler${open ? ' spoiler_open' : ''}`}>
                <div className="spoiler__head" onClick={this._handleToggle}>
                    <ChevronDownIcon label={open ? 'close' : 'open'} />
                    {label}
                </div>
                <div className="spoiler__body">
                    {open ? children : null}
                </div>
            </div>
        );
    }

    _handleToggle = () => {
        this.setState(prevState => {
            return {
                open: !prevState.open
            };
        });
    }
}