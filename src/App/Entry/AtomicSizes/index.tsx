import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import { getSizesReport, getExternalSizesByIdx } from '../../../utils/data';
import { IResourcesUsage } from '../../../utils/main';

import Size from '../../Size';

import './style.less';
import Table, { ITableRow } from '../../Table';
import Spoiler from '../../Spoiler/index';

interface IAtomicSizesProps {
    entryPointIdx: number;
    resourcesUsageMap: IResourcesUsage;
}

class AtomicSizes extends Component<IAtomicSizesProps> {
    tableHead = [
        {
            key: 'name',
            content: 'Name',
            isSortable: true,
        },
        {
            key: 'size',
            content: 'Resource size',
            isSortable: true,
        },
        {
            key: 'percent',
            content: '% of entry point',
            isSortable: true,
        },
    ];

    countPercent(resourceSize: number, entryPointExternalSize: number) {
        return (resourceSize / entryPointExternalSize) * 100;
    }

    render() {
        const { resourcesUsageMap, entryPointIdx } = this.props;

        const entryPointExternalSizes = getExternalSizesByIdx(entryPointIdx);
        const sizesReport = getSizesReport();

        const tableRows = Object.keys(resourcesUsageMap).reduce(
            (rows, resourceKey) => {
                const resourceUsage = resourcesUsageMap[resourceKey];

                const size = sizesReport.all[resourceKey].total;
                const percentFromEntryPoint = this.countPercent(size, entryPointExternalSizes.externalSize.total);

                const consumersTableHead = [
                    {
                        key: 'webresourceKey',
                        content: 'Webresource key',
                        isSortable: true,
                    },
                ];

                const consumersTableRows = resourceUsage.dependencyFor.map(webresource => {
                    return [
                        {
                            key: 'webresourceKey',
                            content: <Link to={`/entry/${entryPointIdx}/resource/${webresource}`}>{webresource}</Link>,
                        },
                    ];
                });

                rows.push([
                    {
                        key: 'name',
                        content: (
                            <>
                                {resourceKey}
                                <Spoiler label="Consumers">
                                    <Table head={consumersTableHead} rows={consumersTableRows} />
                                </Spoiler>
                            </>
                        ),
                        value: resourceKey,
                    },
                    {
                        key: 'size',
                        content: <Size content={size} />,
                        value: size,
                    },
                    {
                        key: 'percent',
                        content: <span>{percentFromEntryPoint.toFixed(2)}%</span>,
                        value: percentFromEntryPoint,
                    },
                ]);

                return rows;
            },
            [] as Array<Array<ITableRow>>
        );

        return (
            <div className="atomic-sizes">
                <Table label="Atomic sizes" filterKey="name" head={this.tableHead} rows={tableRows} />
            </div>
        );
    }
}

export default AtomicSizes;
