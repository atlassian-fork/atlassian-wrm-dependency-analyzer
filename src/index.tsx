import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter as Router } from 'react-router-dom';
import App from './App';

const container = document.querySelector('.app-root');

export default ReactDOM.render(
    <Router hashType="noslash">
        <App/>
    </Router>
    , container);
