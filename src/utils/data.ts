import difference from 'lodash/difference';
import uniq from 'lodash/uniq';
import get from 'lodash/get';
import {
    IAsyncChunkDependencyDescriptor,
    IChunkDependencyDescriptor,
    IEntryDependenciesDescriptor,
    IExternalSizesReport,
    ISizesReportDescriptor,
    WebresourceKey,
} from '../App/types';

/**
 * Global variable
 */
declare const REPORTS: {
    ENTRY_POINT_REPORTS: Array<IAsyncChunkDependencyDescriptor>;
    SIZES_REPORT: ISizesReportDescriptor;
    EXTERNAL_SIZES_REPORT: Array<IExternalSizesReport>;
    DEPENDENCIES_REPORT: Array<IEntryDependenciesDescriptor>;
};

export const getEntryPointsReport = () => REPORTS.ENTRY_POINT_REPORTS;
export const getSizesReport = () => REPORTS.SIZES_REPORT;
export const getExternalSizesReport = () => REPORTS.EXTERNAL_SIZES_REPORT;
export const getDependenciesReport = () => REPORTS.DEPENDENCIES_REPORT;

export const getEntryPointByIdx = (idx: number) => {
    const entryPoint = REPORTS.ENTRY_POINT_REPORTS[idx];

    if (!entryPoint) {
        console.warn(`Can not find entry point with index ${idx}`);
    }

    return entryPoint;
};

export const getChunkByIdx = (entryPointIdx: number, chunkIdx: number) => {
    const entryPoint = getEntryPointByIdx(entryPointIdx);

    return entryPoint.async[chunkIdx];
}

export const getDependenciesOfDependencies = (entryPointIdx: number, chunkIdx?: number | WebresourceKey) => {
    const depenenciesReport = getDependenciesReport();
    const dependencies = chunkIdx ? get(depenenciesReport, [entryPointIdx, 'async', chunkIdx, 'result']) : get(depenenciesReport, [entryPointIdx, 'result']);
    const {
        directDependenciesPerWebresource,
        indirectDependenciesPerWebresource,
        uniqDependenciesPerWebresource,
    } = dependencies;

    const getDeps = (deps: any) => {
        if (!deps) {
            return [];
        }
        return deps.reduce((acc: Array<WebresourceKey>, item: any) => {
            if (!item || !item.dependencies) {
                return acc;
            }

            return acc.concat(item.dependencies);
        }, [] as Array<WebresourceKey>);
    }

    const directDeps = getDeps(directDependenciesPerWebresource);
    const indirectDeps = getDeps(indirectDependenciesPerWebresource);
    const uniqDeps = getDeps(uniqDependenciesPerWebresource);

    return uniq([...directDeps, ...indirectDeps, ...uniqDeps]);
} 

export const getExternalSizesByIdx = (idx: number) => {
    return getExternalSizesReport()[idx];
};

export const getWebresourceByKey = (entryPoint: IChunkDependencyDescriptor, key: WebresourceKey) => {
    const result = entryPoint.result.find(webresource => webresource.webresourceKey === key);

    if (!result) {
        console.error(`Can not find resource "${key}" in entry point ${entryPoint.name}`);
    }

    return result;
};

export const calculateSizeOfDependencies = (dependenciesList: Array<WebresourceKey>) => {
    const sizesReport = getSizesReport();

    return dependenciesList.reduce((sum, webresourceKey) => {
        return sum + sizesReport.all[webresourceKey].total;
    }, 0);
};

interface IIntersectionsDescriptor {
    webresourceKeys: Array<WebresourceKey>;
    dependencies: Array<WebresourceKey>;
    size: number;
}

export const getAtomicDependencies = (entryPoint: IChunkDependencyDescriptor) => {
    return uniq(
        entryPoint.result.reduce(
            (acc, webresource) => {
                return acc.concat(webresource.js.resources);
            },
            [] as Array<WebresourceKey>
        )
    );
};

export const getSharedDependencies = (entryPoint: IChunkDependencyDescriptor, resourceKey: WebresourceKey) => {
    const intersections = getIntersections(entryPoint, resourceKey);

    return uniq(
        intersections.reduce((acc, intersection) => acc.concat(intersection.dependencies), [] as Array<WebresourceKey>)
    );
};

export const getUniqDependencies = (entryPoint: IChunkDependencyDescriptor, resourceKey: WebresourceKey) => {
    const sharedDependencies = getSharedDependencies(entryPoint, resourceKey);
    const webresource = getWebresourceByKey(entryPoint, resourceKey);

    if (!webresource) {
        return [];
    }

    const resourceDependencies = webresource.js.resources.filter(d => d !== resourceKey);

    return difference(resourceDependencies, sharedDependencies);
};

export const getIntersections = (entryPoint: IChunkDependencyDescriptor, resourceKey: WebresourceKey) => {
    const webresource = getWebresourceByKey(entryPoint, resourceKey);
    
    if (!webresource) {
        return [];
    }

    const webresourceDependencies = webresource.js.resources;

    return entryPoint.result.reduce(
        (intersections, externalDependency) => {
            if (externalDependency.webresourceKey !== resourceKey) {
                const dependencyIntersections = webresourceDependencies.filter(dependency =>
                    externalDependency.js.resources.includes(dependency)
                );

                if (dependencyIntersections.length === 0) {
                    return intersections;
                }

                intersections.push({
                    dependencies: dependencyIntersections,
                    size: calculateSizeOfDependencies(dependencyIntersections),
                    webresourceKeys: [resourceKey, externalDependency.webresourceKey],
                });
            }

            return intersections;
        },
        [] as Array<IIntersectionsDescriptor>
    );
};

export const getWebresourceSummary = (entryPoint: IChunkDependencyDescriptor, resourceKey: WebresourceKey) => {
    const sizesReport = getSizesReport();
    const externalSize = get(sizesReport, ['direct', resourceKey, 'total'], 0);
    const uniqSize = get(sizesReport, ['all', resourceKey, 'total'], 0);

    const uniqDependencies = getUniqDependencies(entryPoint, resourceKey);
    const uniqDependenciesSize = calculateSizeOfDependencies(uniqDependencies);

    let resourceType = 'regular';

    if (!uniqSize && uniqDependenciesSize) {
        resourceType = 'provider';
    }

    if (uniqSize && !uniqDependenciesSize && externalSize == uniqSize) {
        resourceType = 'pure';
    }

    if (!uniqSize && !uniqDependenciesSize && !externalSize) {
        resourceType = 'broken';
    }

    return {
        uniqSize,
        externalSize,
        uniqDependenciesSize,
        uniqDependencies,
        type: resourceType,
    };
};
